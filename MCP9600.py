import os
import time
import smbus

#REF: http://www.qoosky.io//techs/2315d68b2e
i2c = smbus.SMBus(1)
addr = [0x61, 0x63, 0x65, 0x67]

def get():
    i = 0
    value = [0, 0, 0, 0, 0, 0, 0, 0]
    for ch in range(4):
        upper = i2c.read_byte_data(addr[ch], 0x00)
        lower = i2c.read_byte_data(addr[ch], 0x01)
        value[ch*2] = upper
        value[ch*2 +1] = lower
    return value

def sanity_check(addr):
    san = 0x20 #sanity check register
    if bus.read_byte_data(addr, san) == 0x40:
        return 1
    else:
        return 0

