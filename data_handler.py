import datetime
import numpy as np
import matplotlib.pyplot as plt
from time import sleep
import MCP9600 as i2c_dev

# Data Recording Loop Function
def record(time_step_sec, time_period_min):
    time_sec = 0
    steps = 0
    time_period_sec = time_period_min * 60
    final_step = int(time_period_sec / time_step_sec) + 1
    first_timestamp = datetime.datetime.now()

    # Data stack is 5 x (num. of steps) Numpy array
    data_stack = np.zeros((5, final_step))
    max_temp = 0

    # Variables for graphical output
    t = 0
    dt = time_step_sec
    tlim = time_period_sec
    xmin, xmax = 0, tlim
    t = data_stack[0, :]
    y1, y2, y3, y4 = data_stack[1, :], data_stack[2, :], data_stack[3,:], data_stack[4, :]
    fig, ax = plt.subplots(1,1)
    ax.set_title('Temperatures measured by thermocouples')
    ax.set_xlim((0, tlim))
    ax.set_xlabel('Time (seconds)')
    ax.set_ylabel('Temperature (degree Celsius')
    ch1_raw = np.zeros(5)
    ch2_raw = np.zeros(5)
    ch3_raw = np.zeros(5)
    ch4_raw = np.zeros(5)

    # Main loop
    while time_sec < time_period_sec + 1:
        # Getting the values of each channels
        for trial in range(4):
            ch1_u, ch1_l, ch2_u, ch2_l, ch3_u, ch3_l, ch4_u, ch4_l = i2c_dev.get()
            ch1_raw[trial] = ch1_u * 16 + ch1_l * 0.0625
            ch2_raw[trial] = ch2_u * 16 + ch2_l * 0.0625
            ch3_raw[trial] = ch3_u * 16 + ch3_l * 0.0625
            ch4_raw[trial] = ch4_u * 16 + ch4_l * 0.0625
            sleep(.3)
        ch1 = np.median(ch1_raw)
        ch2 = np.median(ch2_raw)
        ch3 = np.median(ch3_raw)
        ch4 = np.median(ch4_raw)
        
        # sanity_check (minimum temp. = 0 degC)
        if ch1 > 1500:
            ch1 = 0
        if ch2 > 1500:
            ch2 = 0
        if ch3 > 1500:
            ch3 = 0
        if ch4 > 1500:
            ch4 = 0

        # Max value check
        if ch1 > max_temp:
            max_temp = ch1
        if ch2 > max_temp:
            max_temp = ch2
        if ch3 > max_temp:
            max_temp = ch3
        if ch4 > max_temp:
            max_temp = ch4
        # Putting the values into the data stack
        data_stack[0, steps] = time_sec
        data_stack[1, steps] = ch1
        data_stack[2, steps] = ch2
        data_stack[3, steps] = ch3
        data_stack[4, steps] = ch4
        # Showing the values put into the data stack
        print('ch1: '+ "{0:6.2f}".format(ch1) + ' degC,  '\
               + 'ch2: '+ "{0:6.2f}".format(ch2) + ' degC,  '\
               + 'ch3: '+ "{0:6.2f}".format(ch3) + ' degC,  '\
               + 'ch4: '+ "{0:6.2f}".format(data_stack[4][steps]) + ' degC')
        # Graphical plotting with Matplotlib
        y1, y2, y3, y4 = data_stack[1, :], data_stack[2, :], data_stack[3,:], data_stack[4, :]
        ax.set_ylim((0, max_temp * 1.05))
        sct1 = ax.scatter(t, y1, c='royalblue', s=20, label='Ch.1')
        sct2 = ax.scatter(t, y2, c='seagreen', s=20, label='Ch.2')
        sct3 = ax.scatter(t, y3, c='goldenrod', s=20, label='Ch.3')
        sct4 = ax.scatter(t, y4, c='palevioletred', s=20, label='Ch.4')
        if time_sec == 0:
            ax.legend(loc='lower right')
        plt.pause(time_step_sec - 1.5)
        # Counting up
        time_sec += time_step_sec
        steps += 1

    # Finish time in Terminal
    when_finish = datetime.datetime.now()
    print('Measurement finished at ' + when_finish.strftime("%Y/%m/%d %H:%M:%S"))

    # Save as CSV named by Timestamp
    label = 'Time(sec),Ch1(degC),Ch2(degC),Ch3(degC)Ch4(degC)'
    name_of_csv = first_timestamp.strftime("%Y%m%d%H%M%S") + ".csv"

    np.savetxt(name_of_csv, data_stack, delimiter=",", fmt="%0.2f", header=label)
    sleep(3)
    print(name_of_csv + ' is created!')
    print('All of the value measured is rounded with 2 decmal places.')

