"""
    main.py
        Copyright(c) 2017 Hitoshi "Hugh" Yoshiki
    
    yoshiki.h.aa@m.titech.ac.jp
        or vitam.regit.fortuna@gmail.com

    This software is released under the MIT License.
    http://opensource.org/licenses/mit-license.php

"""

import datetime
from time import sleep
import data_handler as dh
import messages as msg
#import MCP9600 as i2c_dev1


msg.welcome()

#main loop start
while True:

#Initial settings
    time_step = 15 #sec
    time_period = 60 #min

#time_step, time_period input
    ts_str = msg.inputTimeStep()
    tp_str = msg.inputTimePeriod()
    time_step = int(ts_str)
    time_period = int(tp_str)

#    flag_sr = msg.startRecord(ts_str, tp_str)
#Input Y/y for Starting record (return 1), otherwise return 0 
#    if flag_sr == 1:
    #Data handler starts y min recording with x sec steps -> dh.record(x,y)
    dh.record(time_step, time_period)
#    else:
#        pass   

    msg.adjourn()
