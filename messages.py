import sys
from time import sleep

def welcome():
    print("================================================")
    print("*                                              *")
    print("*    Raspberry Pi x I2C Thermometer Application*")
    print("*                      \"i2cQuadThermometer\"    *")
    print("*    Copyright(c) 2017 Hitoshi \"Hugh\" Yoshiki  *")
    print("*        yoshiki.h.aa@m.titech.ac.jp    or     *")
    print("*        vitam.regit.fortuna@gmail.com         *")
    print("*                                              *")
    print("================================================")
    print("\n\n\n")
def adjourn():
    print("\n\nThe program is restarting.\nAre you OK to exit this program?\n")
    ex = input('Exit: [(Y)es/(n)o]: ')
    if ex == 'Y' or ex == 'y':
        sys.exit()
    else:
        print('\n\n\n')
        print('Now program restarting...')
        sleep(1)
        print('\n\n\n')
        welcome()
        return 1

def inputTimeStep():
    print("Please input measurement parameters:\n")
    ts = input('Time step (sec) [ 3 or more is recommended ] : ')
    return ts
def inputTimePeriod():
    tp = input('Time period (min) [ 5 or more is reccomended ]: ')
    return tp
def startRecord(ts, tp):
    print('Now ready for measurement. \nAre you OK to start?')
    print("Time step: " + str(ts) + "(sec) | Time period: " + str(tp) + "(min)")
    yn = input('[(Y)es/(n)o]: ')
    if yn == 'Y' or yn == 'y':
        return 1
    else:
        return 0
