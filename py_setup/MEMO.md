/ memo for python environment setup /


### (1) pyenv setup ###

git clone https://github.com/yyuu/pyenv.git ~/.pyenv

echo 'export PYENV_ROOT="$HOME/.pyenv"' >> ~/.bash_profile
echo 'export PATH="$PYENV_ROOT/bin:$PATH"' >> ~/.bash_profile
echo 'eval "$(pyenv init -)"' >> ~/.bash_profile
exec $SHELL

change "bash_profile" to "bashrc" when the system is Ubuntu

### (2) Python Install ###

pyenv install -l
pyenv install 3.4.6
pyenv global 3.4.6

### (3) Pip packages Install ###

pip install matplotlib numpy drawnow


