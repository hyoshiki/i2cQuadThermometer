#TODO: Reconsider the algorithm
#TODO: The RT plot is no longer required
#TODO: Repetitive data plotting with np.matrix would be awesome

import numpy as np
import matplotlib.pyplot as plt

def rtplot(timestep_sec, period_min, initial_value):
    dt = timestep_sec
    tlim = period_min * 60
    fig, ax = plt.subplots(1, 1)
#    t = np.arange(0, tlim, dt)
    t = 0
    y = initial_value
    lines, = ax.plot(t,y)

    while t < tlim + dt:
        t += dt
        y += 1
        lines.set_data(t,y)
        ax.set_xlim((0, t.max()))
        ax.set_ylim((initial_value, y.max() + 2))
        plt.pause(.1)


if __name__=="__main__":
    rtplot(1, 3, 0)

